
$(document).ready(function () {
    // services navbar section
    $('.menu-navbar').click(function () {
        $(this)
            .addClass('active')
            .siblings()
            .removeClass('active')
            .closest('.services')
            .find('.menu-content')
            .removeClass('active')
            .eq($(this).index())
            .addClass('active');
    });

    $('.amazing-list-item').click(function () {
        $(this)
            .addClass('active')
            .siblings()
            .removeClass('active');
    });

    $('.amazing-list-item').click(function () {
        let category = $(this).attr('data-target');

        $('.amazing-img-item').addClass('hide');
        setTimeout(function () {
            $(`.${category}`).removeClass('hide');
        }, 600);
    });


    $('.loader').hide();
    $('.amazing-img-item').hide();
    $('.amazing-img-item').slice(0, 12).show();
    $('.load-more-btn').click(function () {
        $('.loader').show();
        setTimeout(() => {
            $('.amazing-img-item:hidden').slice(0, 12).show();
            if (!$('.amazing-img-item').is(':hidden')) {
                $(this).hide();
            }
            $('.loader').hide();
        }, 2000);
    });

    $(function(){

        $('.pages .tabs li').on('click', function(){
            // Variable used to navigate through the slide class
            let $tab = $(this).closest('.pages');
            // Finds the button that has the class tab-active and removes it
            $tab.find('.tabs li.tab-active').removeClass('tab-active');
            $tab.find('.tabs li.review-nav-img').removeClass('review-nav-img-active');
            //Adds the class tab-active to the button that was clicked
            $(this).addClass('tab-active');
            $(this).addClass('review-nav-img-active');
            // Variable to grab the rel attribute of the slide list
            let $newSlide = $(this).attr('rel');
            // Finds the slide that is currently active and hides it
            $tab.find('.page-item.page-visible').fadeOut(300, nextPanel);
            // Defining the callback that is activated when hiding active slide
            function nextPanel(){
                // Removes visible class from whichever slide is visible
                $(this).removeClass('page-visible');
                // Matches rel value with the corresponding slide
                $('#' +$newSlide).fadeIn(300, function(){
                    // Displays that slide
                    $(this).addClass('page-visible');
                });
            };
        });
    });
    $(function(){

        //Creating text slides
        // Controller for the text slides
        $('.slides .tabs li').on('click', function(){
            // Variable used to navigate through the slide class
            let $tab = $(this).closest('.slides');
            // Finds the button that has the class tab-active and removes it
            $tab.find('.tabs li.tab-active').removeClass('tab-active');
            $tab.find('.tabs li.review-nav-img').removeClass('review-nav-img-active');
            //Adds the class tab-active to the button that was clicked
            $(this).addClass('tab-active');
            $(this).addClass('review-nav-img-active');
            // Variable to grab the rel attribute of the slide list
            let $newSlide = $(this).attr('rel');
            // Finds the slide that is currently active and hides it
            $tab.find('.slide-item.slide-visible').fadeOut(300, nextPanel);
            // Defining the callback that is activated when hiding active slide
            function nextPanel(){
                // Removes visible class from whichever slide is visible
                $(this).removeClass('slide-visible');
                // Matches rel value with the corresponding slide
                $('#' +$newSlide).fadeIn(300, function(){
                    // Displays that slide
                    $(this).addClass('slide-visible');
                });
            };
        });
    });
